<?php

abstract class Module {

    private $name;
    private $wrapper;

    function getName() {
        return $this->name;
    }

    function getWrapper() {
        return $this->wrapper;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setWrapper($wrapper) {
        $this->wrapper = $wrapper;
    }
}
?>

