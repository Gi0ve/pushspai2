<?php
include_once './module.php';

class Pushover extends Module {

    public function __construct() {
        $this->setWrapper(new PushoverWrapper());
        $this->setName("Pushover");
    }

    public function getAccount() {
        $account = new PushoverAccount($this->getWrapper());
        return $account;
    }

}

class PushoverAccount extends Account {

    public $token = "";
    public $params = array("nome"=>"String","token" => "String");

    function getToken() {
        return $this->token;
    }

    function setToken($token) {
        $this->token = $token;
    }

}

class PushoverWrapper implements Wrapper {

    function __construct() {
        
    }

    public function sendMessage($title, $text, $sender, $receiver) {
        include_once './mysql_parameters.php';
        $token = $sender->getToken();
        $to = $token . "@api.pushover.net";
        include 'mail_parameters.php';
        require 'PhpMailer/class.phpmailer.php';
        require 'PhpMailer/class.smtp.php';
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $body = $text;

        $mail->IsSMTP();
        $mail->Host = MAIL_HOST;

        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = true;

        $mail->Username = MAIL_USERNAME;
        $mail->Password = MAIL_PASSWORD;

        $mail->SetFrom("", "");
        $mail->Subject = $title;
        $mail->MsgHTML($body);
        
        $mail->AddAddress($to, $title);

        $mail->send();

    }

}

$module = new Pushover();
?>