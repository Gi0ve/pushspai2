<?php

include_once './module.php';

class BoxCar extends Module {

    public function __construct() {
        $this->setWrapper(new BoxCarWrapper());
        $this->setName("BoxCar");
    }

    public function getAccount() {
        $account = new BoxCarAccount($this->getWrapper());
        return $account;
    }
}

class BoxCarAccount extends Account {

    public $token = "";
    
    public $params=array("nome"=>"String","token"=>"String");

    function getToken() {
        return $this->token;
    }

    function setToken($token) {
        $this->token = $token;
    }

}

class BoxCarWrapper implements Wrapper {

    function __construct() {
        
    }

    public function sendMessage($title, $text, $sender, $receiver) {
        include_once './mysql_parameters.php';
        $token = $sender->getToken();
        curl_setopt_array(
                $chpush = curl_init(), array(
            CURLOPT_URL => "https://new.boxcar.io/api/notifications",
            CURLOPT_POSTFIELDS => array(
                "user_credentials" => "$token",
                "notification[title]" => "$title",
                "notification[long_message]" => "<b>$text</b>",
                "notification[sound]" => "bird-1",
            )
                )
        );
        $ret = curl_exec($chpush);
        curl_close($chpush);
    }

}

$module = new BoxCar();
?>