<?php

/*ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
*/

$a = new API($_REQUEST['request']);
$a->loadModules();
echo $a->processAPI();

class API {

    protected $method = '';
    protected $endpoint = '';
    protected $verb = '';
    protected $args = Array();
    protected $file = Null;
    protected $modules = Array();

    public function __construct($request) {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");
        $this->args = explode('/', rtrim($request, '/'));
        $this->endpoint = array_shift($this->args);
        if (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
            $this->verb = array_shift($this->args);
        }

        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }

        switch ($this->method) {
            case 'DELETE':
            case 'POST':
                $this->request = $this->_cleanInputs($_POST);
                break;
            case 'GET':
                $this->request = $this->_cleanInputs($_GET);
                break;
            case 'PUT':
                $this->request = $this->_cleanInputs($_GET);
                $this->file = file_get_contents("php://input");
                break;
            default:
                $this->_response('Invalid Method', 405);
                break;
        }
    }

    public function processAPI() {
        if ((int) method_exists($this, $this->endpoint) > 0) {
            return $this->{$this->endpoint}($this->args);
        }
        return $this->_response("No Endpoint: $this->endpoint", 404);
    }

    public function loadModules() {
        $list = scandir("modules");
        foreach ($list as $value) {
            $temp = explode(".", $value);
            $temp = $temp[0];

            $this->modules[] = $temp;
        }
    }

    public function getModules() {
        return $this->modules;
    }

    private function _response($data, $status = 200) {
        header("HTTP/1.1 " . $status . " " . $this->_requestStatus($status));
        return json_encode($data);
    }

    private function _cleanInputs($data) {
        $clean_input = Array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->_cleanInputs($v);
            }
        } else {
            $clean_input = trim(strip_tags($data));
        }
        return $clean_input;
    }

    private function _requestStatus($code) {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');

        return ($status[$code]) ? $status[$code] : $status[500];
    }

    private function getAccTypes() {
        $modules = $this->getModules();
        $temp = array();
        foreach ($modules as $key => $value) {
            if (!empty($value)) {
                $temp[] = $modules[$key];
            }
        }
        $modules = $temp;
        return $this->_response($modules);
    }

    public function includeAll() {
        $modules = $this->getModules();
        $modules = array_filter($modules);
        foreach ($modules as $value) {
            include_once "modules/$value.php";
        }
    }

    private function getAccounts() {
        if (Session::getInstance()->isLogged() == FALSE) {
            return $this->_response("errore, non sei loggato", 401);
        } else {
            $modules = $this->getModules();
            $modules = array_filter($modules);
            foreach ($modules as $value) {
                include "modules/$value.php";
            }
            $user = Session::getInstance()->getUser();
            $user->reload();
            $temp = $user->getAccounts();
            $temp2 = array();
            foreach ($temp as $value) {
                $temp2[] = $value->getName();
            }
            return $this->_response($temp2);
        }
    }

    private function logout() {
        if (Session::getInstance()->isLogged() == TRUE) {
            session_destroy();
            $this->_response("disconnesso");
        } else {
            $this->_response("non sei loggato", 401);
        }
    }

    private function accParameters($args) {
        $name = $args[0];
        if (array_search($name, $this->getModules()) != FALSE) {
            $str = "modules/$name.php";
            include "$str";
            $temp = $module->getAccount()->params;
            return $this->_response($temp);
        } else {
            $this->_response("modulo inesistente");
        }
    }

    private function addAccount($args) {
        $name = $args[0];
        $type = $args[1];
        $default = $args[2];
        $str = $type . ".php";
        include "modules/$str";
        $account = $module->getAccount();
        $this->includeAll();
        unset($args[0]);
        unset($args[1]);
        unset($args[2]);
        $array = array();
        foreach ($args as $value) {
            $temp = explode("=", $value);
            $array["$temp[0]"] = $temp[1];
        }
        $user = Session::getInstance()->getUser();
        $result = $user->addAccount($name, $account, $array);
        if ($result == true) {
            if ($default == 1) {            
                $result = $user->setDefault($name);
            }
            return $this->_response(TRUE);
        } else {
            return $this->_response("errore, account già esistente");
        }
    }

    private function setDefaultAccount($args) {
        $name = $args[0];
        $modules = $this->getModules();
        $modules = array_filter($modules);
        foreach ($modules as $value) {
            include "modules/$value.php";
        }
        if (Session::getInstance()->isLogged()) {
            $user = Session::getInstance()->getUser();
            $result = $user->setDefault($name);

            if ($result == false) {
                return $this->_response("l'account non esiste");
            } else {
                return $this->_response(true);
            }
        } else {
            return $this->_response("non sei loggato", 401);
        }
    }

    private function write($args) {
        if (Session::getInstance()->isLogged() == TRUE) {
            $user = $args[0];
            $title = $args[1];
            $text = $args[2];

            $this->includeAll();
            $receiver = User::getUserByName($user);
            if ($receiver == FALSE) {
                return $this->_response("L'utente a cui vuoi inviare il messaggio non esiste", 406);
            } else {

                $sender = Session::getInstance()->getUser();
                $account = $receiver->getDefault();
                $temp = $receiver->getAccounts();
                foreach ($temp as $key => $value) {
                    if ($value->getName() == $account) {
                        $account = $temp[$key];
                    }
                }
                if ($account == false) {
                    return $this->_response("account di default non specificato");
                } else {
                    $message = new Message($title, $text);
                    $return = $sender->writes($message, $receiver, $account);
                    return($this->_response($return));
                }
            }
        } else {
            return $this->_response("Non sei loggato", 406);
        }
    }

    private function login($args) {
        if (Session::getInstance()->isLogged() == TRUE) {
            return $this->_response("Sei gia' loggato", 401);
        } else {
            $username = $args[0];
            $password = hash("sha512", $args[1]);
            $conn = mysql::getInstance()->getConnection();
            $result = $query = "SELECT * FROM users WHERE username='$username' AND password='$password'";
            $result = mysqli_query($conn, $query);
            if (mysqli_affected_rows($conn) > 0) {
                $user = User::getUserByName($username);
                $session = Session::getInstance()->setUser($user);
                return $this->_response("Accesso effettuato");
            } else {
                return $this->_response("Dati di accesso errati", 406);
            }
        }
    }

    private function register($args) {

        $username = $args[0];
        $password = $args[1];
        $email = $args[2];

        if (isset($username) && isset($password) && isset($email)) {

            if (User::userExists($username) == false) {
                $u = new User();
                $u->setUsername($username);
                $u->setPassword($password);
                $u->setEmail($email);
                $result = $u->save();
                return $this->_response("Registrazione effettuata", 200);
            } else {
                return $this->_response("Utente con questo nome gia registrato", 406);
            }
        } else {
            return $this->_response("Riempi tutti i campi prima di inviare il form", 406);
        }
    }

}

class Message {

    private $id;
    private $title;
    private $text;

    function __construct($title, $text) {
        $this->title = $title;
        $this->text = $text;
    }

    function getId() {
        return $this->id;
    }

    function getTitle() {
        return $this->title;
    }

    function getText() {
        return $this->text;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setText($text) {
        $this->text = $text;
    }

    public function getType() {
        return "Message";
    }

    public function save() {
        if (isset($this->id)) {
            
        }
    }

    static function getMessage($id) {
        $query = "SELECT * FROM messages WHERE id=$id";
        $conn = Mysql::getInstance()->getConnection();
        $res1 = mysqli_query($conn, $query);
        if (mysqli_affected_rows($conn) > 0) {
            $arr = mysqli_fetch_array($res1, MYSQLI_ASSOC);
            $tempid = $res1['id'];
            $temptitle = $res1['title'];
            $temptext = $arr['text'];
            $message = new Message($tempid, $temptitle, $temptext);
            return $message;
        } else {
            return NULL;
        }
    }

}

class User {

    private $id = null;
    private $username = "";
    private $password = "";
    private $email = "";
    private $accounts = array();
    private $default;

    public function __construct() {
        
    }

    function setDefault($name) {

        $this->reload();
        $temp = $this->getAccounts();

        $temp2 = array();

        foreach ($temp as $value) {
            $temp2[] = $value->getName();
        }

        if (in_array($name, $temp2) == true) {
            $this->default = $name;
            $this->update();
            return true;
        } else {
            return false;
        }
    }

    function addAccount($name, $account, $params) {
        $this->reload();
        $temp = $this->getAccounts();

        $temp2 = array();

        foreach ($temp as $value) {
            $temp2[] = $value->getName();
        }
        if (in_array($name, $temp2)) {
            return false;
        } else {

            $account->setName($name);
            foreach ($params as $key => $value) {
                $account->setProperty($key, $value);
            }

            $this->accounts[] = $account;

            $this->save();
            return true;
        }
    }

    function getAccounts() {
        return $this->accounts;
    }

    function setAccounts($accounts) {
        $this->accounts = $accounts;
    }

    function getId() {
        return $this->id;
    }

    function getUsername() {
        return $this->username;
    }

    function getPassword() {
        return $this->password;
    }

    function getEmail() {
        return $this->email;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    public function getType() {
        return "User";
    }

    public function getDefault() {
        if (!isset($this->default)) {
            return false;
        } else {
            return $this->default;
        }
    }

    public function reload() {
        $var = User::getUser($this->id);
        $this->username = $var->getUsername();
        $this->password = $var->getPassword();
        $this->email = $var->getEmail();
        $this->default = $var->getDefault();
        $temp = $var->getAccounts();
        if (isset($temp)) {
            $this->setAccounts($temp);
        }
    }

    static function getUser($id) {
        $user = new User();
        $conn = Mysql::getInstance()->getConnection();
        $query = "SELECT * FROM users WHERE id=$id";
        $res1 = mysqli_query($conn, $query);
        if (mysqli_affected_rows($conn) > 0) {
            $user->id = $id;
            $arr = mysqli_fetch_array($res1, MYSQLI_ASSOC);
            $user->username = $arr['username'];
            $user->password = $arr['password'];
            $user->email = $arr['email'];
            $user->default = $arr['default2'];
            if (isset($arr['accounts'])) {
                $user->accounts = unserialize($arr['accounts']);
            }
            return $user;
        } else {
            return NULL;
        }
    }

    static function getUserByName($username) {
        $user = new User();
        $conn = Mysql::getInstance()->getConnection();
        $query = "SELECT * FROM users WHERE username='$username'";
        $res1 = mysqli_query($conn, $query);
        if (mysqli_affected_rows($conn) > 0) {
            $arr = mysqli_fetch_array($res1, MYSQLI_ASSOC);

            $user->id = $arr['id'];
            $user->username = $arr['username'];
            $user->password = $arr['password'];
            $user->email = $arr['email'];
            $user->default = $arr['default2'];
            if (!empty($arr['accounts'])) {
                $user->accounts = unserialize($arr['accounts']);
            }
            if (!empty($arr['default2'])) {
                $user->default = $arr['default2'];
            }
            return $user;
        } else {
            return false;
        }
    }

    static function userExists($username) {
        $conn = mysql::getInstance()->getConnection();

        $query = "SELECT * FROM users WHERE username='$username'";
        $res1 = mysqli_query($conn, $query);
        if (mysqli_affected_rows($conn) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function save() {
        if (!isset($this->id)) {
            $mysql = Mysql::getInstance();
            $conn = $mysql->getConnection();
            $password = hash("sha512", $this->password);
            if (!isset($this->accounts)) {
                $query1 = "INSERT INTO users (username,password,email,default2) VALUES ('$this->username','$password','$this->email','$this->default')";
            } else {
                $temp = serialize($this->accounts);
                $query1 = "INSERT INTO users (username,password,email,accounts,default2) VALUES ('$this->username','$password','$this->email','$temp','$this->default')";
            }
            $result = mysqli_query($conn, $query1);
            if ($result == FALSE) {
                return null;
            } else {
                $this->id = mysqli_insert_id($conn);
                return TRUE;
            }
        } else {
            $this->update();
        }
    }

    private function update() {
        $mysql = Mysql::getInstance();
        $conn = $mysql->getConnection();
        if (isset($this->accounts)) {
            $temp = mysqli_real_escape_string($conn, serialize($this->accounts));
            $query1 = "UPDATE users "
                    . "SET username='$this->username',password='$this->password',email='$this->email',accounts='$temp',default2='$this->default' "
                    . "WHERE id=$this->id";
        } else {
            $query1 = "UPDATE users"
                    . " SET username=$this->username, password=$this->password, email=$this->email,default2='$this->default' "
                    . "WHERE id=$this->id";
        }
        $result = mysqli_query($conn, $query1);
        echo(mysqli_error($conn));
        if ($result == FALSE) {
            return null;
        } else {
            return TRUE;
        }
    }

    public function writes($message, $user, $account) {
        $conn = Mysql::getInstance()->getConnection();
        $sender = Session::getInstance()->getUser();
        if ($message->getType() == "Message" && $user->getType() == "User") {
            $res = $account->getWrapper()->sendMessage($message->getTitle(), $message->getText(), $account, $user);
            if ($res == true) {
                $temp = $user->getAccounts();
                if (isset($temp)) {
                    $query = "INSERT INTO messages ('title','text','fk_sender','fk_receiver') values ('$message->getTitle','$message->getText','" . Session::getInstance()->getUser()->getId() . "','" . $receiver->getId() . "')";
                    $res = mysqli_query($conn, $query);
                    if (mysqli_affected_rows($conn) > 0) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return false;
        }
    }

    public function getMessages() {
        if (isset($this->id)) {
            $conn = Mysql::getInstance()->getConnection();

            $query = "SELECT id FROM messages WHERE fk_sender=$this->id";
            $res = mysqli_query($conn, $query);
            $arr = mysqli_fetch_array($res, MYSQLI_NUM);
            foreach ($arr as $value) {
                $temp = Message::getMessage($value);
                $this->messages[] = $temp;
            }
        } else {
            return NULL;
        }
    }

}

abstract class Account {

    public $name;
    private $params = array();
    private $wrapper;

    function __construct($wrapper) {
        $this->wrapper = $wrapper;
    }

    function getParams() {
        return $this->params;
    }

    function setParams($params) {
        $this->params = $params;
    }

    function getName() {
        return $this->name;
    }

    function setName($name) {
        $this->name = $name;
    }

    function getWrapper() {
        return $this->wrapper;
    }

    function setWrapper($wrapper) {
        $this->wrapper = $wrapper;
    }

    function setProperty($property, $value) {
        $this->$property = $value;
    }

}

class Mysql {

    static private $instance = null;
    private $conn;

    private function __construct() {
        include_once './mysql_parameters.php';

        $this->conn = mysqli_connect(MYSQL_HOST, MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_DB);
    }

    static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new Mysql();
        }
        return self::$instance;
    }

    public function getConnection() {
        return $this->conn;
    }

}

class Session {

    static private $instance;

    private function __construct() {
        session_start();
        if (empty($_SESSION['logged'])) {
            $this->loggedFalse();
            $this->setUser(false);
        }
    }

    static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new Session();
        }
        return self::$instance;
    }

    public function setUser($user) {
        if ($user != false) {
            $_SESSION['user'] = serialize($user);
            $this->loggedTrue();
        }
    }

    public function getUser() {
        return unserialize($_SESSION['user']);
    }

    public function isLogged() {
        if ($_SESSION['logged'] == TRUE) {
            return true;
        } else {
            return false;
        }
    }

    public function loggedTrue() {
        $_SESSION['logged'] = true;
    }

    public function loggedFalse() {
        $_SESSION['logged'] = false;
    }

    public function destroy() {
        session_destroy();
    }

}

interface Wrapper {

    function sendMessage($title, $text, $sender, $receiver);
}

?>